<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">


		<header id="site-head" class="Site-Header" role="banner">
			<div class="site-header-main">
				<div class="site-branding">
					<?php twentysixteen_the_custom_logo(); ?>

					<?php if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php endif; ?>
				</div>

					<div id="site-header-menu" class="site-header-menu">
<!--						--><?php //if ( has_nav_menu( 'primary' ) ) : ?>
<!--							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="--><?php //esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?><!--">-->
<!--								--><?php
//									wp_nav_menu( array(
//										'theme_location' => 'primary',
//										'menu_class'     => 'primary-menu',
//									 ) );
//								?>
<!--							</nav>-->
<!--						--><?php //endif; ?>
<!--						--><?php //endif; ?>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu',
						) );
						wp_nav_menu( array(
							'theme_location' => 'rubric',
							'menu_class'     => 'primary-menu',
						) );
						get_search_form();
						?>

					</div>

			</div>

		</header>

		<div id="content" class="site-content">
