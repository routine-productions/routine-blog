<?php
get_header();
the_post();
?>
<section id="post-section" class="Post-Section">

	<h1><?= the_title()?></h1>

	<div id="post-content" class="Post-Content">

		<?= the_content()?>

	</div>

</section>
<div class="Coments">
	<?php
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	};
//	wp_get_current_commenter();
	?>

</div>


<?php get_footer(); ?>
