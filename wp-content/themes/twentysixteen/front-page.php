<?php

get_header(); ?>

<div id="main-page" class="Main-Page">
    <main id="main" class="site-main" role="main">
        <h1>Главная</h1>
        <?php
        the_post();
        get_template_part( 'template-parts/content', 'page' );

        ?>

    </main>


</div>

<?php
get_sidebar();
get_footer(); ?>
